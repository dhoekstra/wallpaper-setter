# Wallpaper setter

A simple to tool for managing and setting wallpapers on \*nix systems with pywal.
This package provides the `wpsetter` cli utility.
